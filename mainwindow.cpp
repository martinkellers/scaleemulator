#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QTcpSocket>
#include <QTimer>
#include <string>
#include <QDateTime>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_Server = new QTcpServer(this);
    m_isConnected = false;

    // setup timer for firing feedback weights
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(fireWeightFeedbackEvent()));

    //default values
    ui->txt_feedbackRate->setText("1000");
    m_feedbackRate = ui->txt_feedbackRate->text().toInt();
    m_timer->start(m_feedbackRate);
    logMsg("Default feedback rate set to "+ QString::number(m_feedbackRate) + "[ms]");

    ui->txt_feedbackWeight->setText("500");
    logMsg("Default feedback weight set to 500 [gr]");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onNewConnectionObtained()
{
    qDebug() << "Got a new connection...";

    m_socket = m_Server->nextPendingConnection();
    connect(m_socket, SIGNAL(disconnected),m_socket, SLOT(deleteLater));
    connect(m_socket, SIGNAL(disconnected),this, SLOT(onDisconnected()));
    connect(m_socket, SIGNAL(readyRead()),this,SLOT(onNewDataReceived()));

    m_isConnected = true;
    ui->lbl_status->setText("Connected");
    logMsg("Connected to client");
}

void MainWindow::onDisconnected()
{
    m_isConnected = false;
    logMsg("Client disconnected...");
}

void MainWindow::onNewDataReceived()
{
    // we only expect ping signals from the client as 1 byte packets
    int bytesAvailable = m_socket->bytesAvailable();
    if(bytesAvailable > 1){
        // ok read the byte
        char byte;
        m_socket->read(&byte,1);
        logMsg("Ping Received");
    }
}

void MainWindow::fireWeightFeedbackEvent()
{

    if(m_isConnected){
        // it is time to send a feedback weight

        // build the string
        //943<HT>grade<HT>length<HT>value<HT>stdev<HT>range<HT>count<HT>delta range<HT>delta stdev<CR><LF>

       // 943<HT>grade<HT>length<HT>value<HT>stdev<HT>range<HT>count<HT>delta range<HT>delta stdev<CR><LF>
        QString weightString;

        //header
        weightString.append("943\t");

        //grade
        weightString.append("0\t");

        //length
        weightString.append("200\t");

        //weight
        QString weight = ui->txt_feedbackWeight->text();
        weightString.append(weight+"\t");

        //stdev
        weightString.append("0\t");

        //range
        weightString.append("0\t");

        //count
        weightString.append("0\t");

        //delta
        weightString.append("0\t");

        //delta stdev
        weightString.append("0\t0\t");


        // terminate string
        weightString.append("\r\n");


        #if QT_VERSION < QT_VERSION_CHECK(5,0,0)
             QByteArray writeBuffer(weightString.toAscii();
        #else
             QByteArray writeBuffer(weightString.toLocal8Bit());
        #endif
        if(m_socket->write(writeBuffer)==writeBuffer.size()){
            logMsg("Feedback event fired...\n");
            logMsg(weightString);
        }
        else
        {
            logMsg("Failed sending packet...\n");
        }

    }

}

void MainWindow::on_btn_startServer_clicked()
{
    connect(m_Server,SIGNAL(newConnection()),this,SLOT(onNewConnectionObtained()));

    if(m_Server->listen(QHostAddress::Any,52254)){
            qDebug() << "Server is listening...!";
            ui->lbl_status->setText("Listening");
            logMsg("Server Listening");
    }
    else{
            qDebug() << "Server could not start...!";
            ui->lbl_status->setText("Server Error");
            logMsg("Server failed to start");
    }

}

void MainWindow::on_btn_Disconnect_clicked()
{
    m_Server->close();
    logMsg("Server Shutdown");
    ui->lbl_status->setText("Server Shutdown");
    m_isConnected = false;
}

void MainWindow::logMsg(QString message)
{
    ui->plainTextEdit->appendPlainText(QDateTime::currentDateTime().toString() +" " +message+"\n");
}


void MainWindow::on_txt_feedbackWeight_editingFinished()
{
    m_feedbackWeight = ui->txt_feedbackWeight->text();
    logMsg("Feedback weight changed to "+ QString(m_feedbackWeight));
}

void MainWindow::on_txt_feedbackRate_editingFinished()
{
    m_feedbackRate = ui->txt_feedbackRate->text().toInt();
    m_timer->setInterval(m_feedbackRate);
    logMsg("Feedback rate changed to "+ QString::number(m_feedbackRate));
}
