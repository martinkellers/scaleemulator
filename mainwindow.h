#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork/QTcpServer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:

    void onNewConnectionObtained();
    void onDisconnected();
    void onNewDataReceived();
    void fireWeightFeedbackEvent();

    void on_btn_startServer_clicked();

    void on_btn_Disconnect_clicked();

    void on_txt_feedbackWeight_editingFinished();

    void on_txt_feedbackRate_editingFinished();

private:

    void logMsg(QString message);
    Ui::MainWindow *ui;

    QTcpServer* m_Server;
    QTcpSocket* m_socket;
    QTimer* m_timer;

    bool m_isConnected;
    unsigned int m_feedbackRate;
    QString m_feedbackWeight;
};

#endif // MAINWINDOW_H
